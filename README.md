# Georges

Advanced IRC bot to deal with APTs: Annoying Persistent Troll. It takes care of
both dedicated and automated trolls without having to ban large subnets or all
Tor users.

An IRC APT is unlike an Advanced Persistent Threat: it's definitely not
advanced and definitely not smart. It is however very persistent to the point
of probably spending most of its time trying to bugger people.

Georges has been created because of a troll that lost more than 3 years of its
non-life spamming and harassing. This occurred on an IRC network where it was
not conceivable to ban too much and where people routinely use Tor. Georges
acts swiftly and denies trolls their core retribution: disrupting communities.

Georges uses a breadth of different techniques to detect such people and ban
them for an appropriate amount of time. Every criteria modifies a per-user
score that is used to determine when to ban and how long to ban. No criteria is
used alone. Criteria include:
* Time between join and message (restricts on-join spam),
* Rough physical location (makes sense for mostly,
* Open proxy, Tor or webchat use,
* Client oddities,
* Mass joins,
* Nickname stealing,
* Bad words and bad nicks.

## Status

Works well.

## Code quality

Low. Most of the code has grown organically in an ad-hoc and on-the-spot
fashion driven purely by troll behaviour at a given moment. With that being
said, it works and is stable.

# OCabot

IRC bot for "#OCaml" on freenode. Georges is based on OCabot.

## Build

- `opam pin add calculon https://github.com/c-cube/calculon.git`
- `make`
- `./ocabot.native`

## License

MIT, see `LICENSE`

